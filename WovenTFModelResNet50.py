import tensorflow as tf
import numpy as np
import os
import pathlib
from keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflow_hub as hub
import datetime

class myCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        if logs.get('accuracy') > 0.90 and logs.get('val_accuracy') > 0.85:
            self.model.stop_training=True

log_dir = r"logs\fit\{}".format(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

train_datagen = ImageDataGenerator(rescale=1.0/255.0,
                                   rotation_range=90,
                                   width_shift_range=0.2,
                                   height_shift_range=0.2,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True,
                                   vertical_flip=True,
                                   fill_mode='nearest')

val_datagen = ImageDataGenerator(rescale=1.0/255.0)

train_generator = train_datagen.flow_from_directory(
        r'D:\Experiment\TFExperiment\TRAIN',
        target_size=(224,224),
        batch_size=4,
        class_mode='categorical')

val_generator = val_datagen.flow_from_directory(
        r'D:\Experiment\TFExperiment\VAL',
        target_size=(224,224),
        batch_size=2,
        class_mode='categorical')

print(train_generator.class_indices.keys())

callbacks = myCallback()

model_transfer = tf.keras.applications.resnet50.ResNet50(
    input_shape=(224,224,3)
)

x = tf.keras.layers.Dense(2048, activation="relu")(model_transfer.output)
x = tf.keras.layers.Dropout(0.2)(x)
x = tf.keras.layers.Dense(5, activation="softmax")(x)
model = tf.keras.Model(model_transfer.input, x)

model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer=tf.keras.optimizers.RMSprop(learning_rate=0.00001),
              metrics=['accuracy'])

history = model.fit(train_generator,
                    validation_data=val_generator,
                    epochs=200,
                    callbacks = [callbacks,tensorboard_callback],
                    verbose=1)


img = image.load_img(r'C:\Users\FarhanNurHakim\Downloads\Screenshot 2022-06-11 102222.jpg', target_size=(224,224))
x = image.img_to_array(img)
x = x/255
x = np.expand_dims(x, axis=0)

images = np.vstack([x])
classes = model.predict(images)
rounded_prediction = np.argmax(classes, axis=-1)
print(classes)
print(rounded_prediction)

model.save('tenunsantara.model')
tf.saved_model.save(model, r'D:\Experiment\TFExperiment\savedmodel')

converter = tf.lite.TFLiteConverter.from_saved_model('D:\Experiment\TFExperiment\savedmodel')

# converter.optimizations = [tf.lite.Optimize.DEFAULT]
# converter.target_spec.supported_types = [tf.float16]

tflite_model = converter.convert()

tflite_model_file = pathlib.Path(r'D:\Experiment\TFExperiment\tmp\tenunsantaramodel.tflite')
tflite_model_file.write_bytes(tflite_model)
