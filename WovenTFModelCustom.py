import tensorflow as tf
import numpy as np
import os
import pathlib
from keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator



class myCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        if logs.get('accuracy') > 0.90:
            self.model.stop_training=True

train_datagen = ImageDataGenerator(rescale=1.0/255.0,
                                   rotation_range=45,
                                   width_shift_range=0.2,
                                   height_shift_range=0.2,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True,
                                   vertical_flip=True,
                                   fill_mode='nearest')
train_generator = train_datagen.flow_from_directory(
        r'D:\Experiment\TFExperiment\TRAIN',
        target_size=(300,300),
        batch_size=2,
        class_mode='categorical')

print(train_generator.class_indices.keys())

callbacks = myCallback()

model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(8, (3,3), activation='relu', input_shape=(300,300,3)),
    tf.keras.layers.MaxPooling2D(2,2),
    tf.keras.layers.Conv2D(32, (3,3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2,2),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(1024, activation = 'relu'),
    tf.keras.layers.Dense(3, activation = 'softmax')
])
model.compile(loss='categorical_crossentropy',
              optimizer=tf.keras.optimizers.RMSprop(),
              metrics=['accuracy'])

history = model.fit(train_generator,
                    epochs=50,
                    steps_per_epoch=20,
                    callbacks = [callbacks],
                    verbose=1)

img = image.load_img(r'C:\Users\FarhanNurHakim\Downloads\luriklurik.jpg', target_size=(300,300))
x = image.img_to_array(img)
x = x/255
x = np.expand_dims(x, axis=0)

images = np.vstack([x])
classes = model.predict(images)
rounded_prediction = np.argmax(classes, axis=-1)
print(classes)
print(rounded_prediction)

# if classes[0]<0.5:
#     print('ulos')
# else:
#     print('lurik')
# model.save('UlosdanLurikmodel.model')
# tf.saved_model.save(model, r'D:\Experiment\TFExperiment\savedmodel')
#
# converter = tf.lite.TFLiteConverter.from_saved_model('D:\Experiment\TFExperiment\savedmodel')
#
# converter.optimizations = [tf.lite.Optimize.DEFAULT]
# converter.target_spec.supported_types = [tf.float16]
#
# tflite_model = converter.convert()
#
# tflite_model_file = pathlib.Path(r'D:\Experiment\TFExperiment\tmp\UlosdanLurikmodel.tflite')
# tflite_model_file.write_bytes(tflite_model)