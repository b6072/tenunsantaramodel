import tensorflow as tf
import numpy as np
import os
import pathlib
from keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator


img = image.load_img(r'C:\Users\FarhanNurHakim\Downloads\luriklurik.jpg', target_size=(300,300))
x = image.img_to_array(img)
print(x)
print('Next Line \n')
x = x/255
print(x)
print('Next Line \n')
x = np.expand_dims(x, axis=0)
print(x)
print('Grand Line \n')
images = np.vstack([x])
print(images)