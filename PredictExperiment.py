import tensorflow as tf
import numpy as np
import os
import pathlib
from keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflow_hub as hub

model = tf.keras.models.load_model("tenunsantara.model")

img = image.load_img(r'C:\Users\FarhanNurHakim\Downloads\bantallurikimages.jpg', target_size=(224,224))
x = image.img_to_array(img)
x = x/255
x = np.expand_dims(x, axis=0)

images = np.vstack([x])
classes = model.predict(images)
rounded_prediction = np.argmax(classes, axis=-1)
print(classes)
print(rounded_prediction)
print()
if rounded_prediction[0] == 0:
    print('Lurik')
elif rounded_prediction[0] == 1:
    print('Sumba')
elif rounded_prediction[0] == 2:
    print('Ulos')
elif rounded_prediction[0] == 3:
    print('Toraja')
else:
    print('gringsing')